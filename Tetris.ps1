Write-Host "Compiling Tetris..."

# Get the script's path
$dir = $MyInvocation.MyCommand.Path
# Get the script's parent dir
$dir = Split-Path $dir -Parent

# Store some dirs
$builddir = "$dir\build"
$libsdir = "$dir\libs"

# Clear the old existing java compiled files
&cmd.exe /c rd /s /q $builddir

# Compile
javac -d "$builddir" .\com\upv\progra\tetris\Tetris.java

# Run the program
java -cp "$libsdir" -cp "$builddir" com.upv.progra.tetris.Tetris