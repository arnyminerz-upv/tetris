package com.upv.progra.tetris;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.*;

public class Tetris {
    private static Board board;
    private static final Runnable runnable = new Runnable() {
        @Override
        public void run() {
            Debugger.ln(Debugger.DEBUG_INFO);
            Debugger.println(Debugger.DEBUG_INFO, "--- PAINTING ---");
            board.tick();
        }
    };

    public static final short GAME_WIDTH = 10;
    public static final short GAME_HEIGHT = 20;
    /**
     * The fps of the game when no down key is pressed
     */
    public static final int GAME_REGULAR_FPS = 2;
    /**
     * The multiplier for the fps when the down key is pressed
     */
    public static final float GAME_SPEED_MULT = 3;
    public static final short BLOCK_SIZE = 30;

    private static Clock clock = new Clock(GAME_REGULAR_FPS, runnable);

    public static void main(String[] args) {
        JFrame f = new JFrame();
        JPanel p = new JPanel() {
            /**
             * The panel serial version UUID
             */
            private static final long serialVersionUID = 1351854566423023807L;

            @Override
            protected void paintComponent(Graphics g) {
                super.paintComponent(g);
            }
        };
        f.setSize(new Dimension(GAME_WIDTH * BLOCK_SIZE, GAME_HEIGHT * BLOCK_SIZE));
        f.add(p);
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        f.setVisible(true);
        f.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {
            }

            @Override
            public void keyPressed(KeyEvent e) {
                int c = e.getKeyCode();
                switch (c) {
                    case KeyEvent.VK_RIGHT:
                        board.moveRight();
                        break;
                    case KeyEvent.VK_LEFT:
                        board.moveLeft();
                        break;
                    case KeyEvent.VK_UP:
                        board.rotate();
                        break;
                    case KeyEvent.VK_DOWN:
                        clock.setFps((int) (GAME_REGULAR_FPS * GAME_SPEED_MULT));
                        break;
                }
            }

            @Override
            public void keyReleased(KeyEvent e) {
                int c = e.getKeyCode();
                if (c == KeyEvent.VK_DOWN)
                    clock.setFps(GAME_REGULAR_FPS);
            }
        });
        board = new Board(p);
        board.createNewPiece();
        clock.start();
    }
}