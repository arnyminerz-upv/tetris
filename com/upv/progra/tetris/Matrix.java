package com.upv.progra.tetris;

import java.lang.reflect.Array;

public class Matrix<T> {
    /**
     * The contents of the matrix
     */
    private T[][] matrix;

    private Class<T> type;
    private T negativeType;

    private int width = -1;
    private int height = -1;

    /**
     * Initializes the matrix
     * 
     * @param type         The generic class
     * @param matrix       The matrix
     * @param negativeType The value that represents negative, false for booleans,
     *                     for example
     */
    public Matrix(Class<T> type, T[][] matrix, T negativeType) {
        this.matrix = matrix;
        this.type = type;
        this.negativeType = negativeType;
        flow();
    }

    /**
     * Initializes the matrix
     * 
     * @param matrix The matrix
     */
    public Matrix(Matrix<T> matrix) {
        this.matrix = matrix.matrix;
        this.type = matrix.type;
        this.negativeType = matrix.negativeType;
        // No need to flow, since the matrix had been initialized
    }

    private T[][] populate(T[][] matrix, int max, T nullType) {
        T[][] newMatrix = (T[][]) Array.newInstance(type, new int[] { max, max });
        for (int y = 0; y < max; y++) {
            for (int x = 0; x < max; x++) {
                try {
                    newMatrix[y][x] = matrix[y][x];
                } catch (ArrayIndexOutOfBoundsException e) {
                    newMatrix[y][x] = nullType;
                }
            }
        }
        return newMatrix;
    }

    /**
     * Fills the matrix to have the same width than height
     */
    private void flow() {
        int max = matrix.length;
        // First, find the maximum dimension x or y
        for (int c = 0; c < matrix.length; c++)
            max = Math.max(matrix[c].length, max);

        // Create the new matrix
        T[][] matrix = populate(this.matrix, max, negativeType);

        this.matrix = matrix;

        computeSize();
    }

    /**
     * Rotates the matrix Made by Dhiraj
     * (https://www.devglan.com/java-programs/java-program-matrix-rotation)
     */
    public void rotate() {
        int row = matrix.length;
        // first find the transpose of the matrix.
        for (int i = 0; i < row; i++) {
            for (int j = i; j < row; j++) {
                T temp = matrix[i][j];
                matrix[i][j] = matrix[j][i];
                matrix[j][i] = temp;
            }
        }
        // reverse each row
        for (int i = 0; i < row; i++) {
            for (int j = 0; j < row / 2; j++) {
                T temp = matrix[i][j];
                matrix[i][j] = matrix[i][row - 1 - j];
                matrix[i][row - 1 - j] = temp;
            }
        }

        computeSize();
    }

    /**
     * Shows the matrix through the terminal
     */
    public void log() {
        Debugger.println(Debugger.DEBUG_INFO, "Matrix height: %d", height());
        Debugger.println(Debugger.DEBUG_INFO, "Matrix width: %d\n", width());
        for (int i = 0; i < length(); i++) {
            for (int j = 0; j < get(i).length; j++)
                Debugger.print(Debugger.DEBUG_INFO, (get(i)[j] == negativeType ? " " : "#"));
            Debugger.ln(Debugger.DEBUG_INFO);
        }
        Debugger.ln(Debugger.DEBUG_INFO);
    }

    /**
     * Gets the length of the matrix
     * 
     * @return The matrix's size
     */
    public int length() {
        return matrix.length;
    }

    public T[] get(int index) {
        return matrix[index];
    }

    /**
     * Computes the matrix's size and stores it for getting it later
     */
    private void computeSize() {
        // The greatest x and y position with a true value
        int greatestTrueX = -1;
        int greatestTrueY = -1;

        // Scan the matrix
        for (int y = 0; y < length(); y++) {
            T[] row = get(y);
            for (int x = 0; x < row.length; x++) {
                if (row[x] != negativeType) {
                    greatestTrueY = Math.max(y, greatestTrueY);
                    greatestTrueX = Math.max(x, greatestTrueX);
                }
            }
        }

        width = greatestTrueX + 1;
        height = greatestTrueY + 1;
    }

    /**
     * Gets the matrix's height
     * 
     * @return The matrix's height
     * @throws IllegalStateException When the matrix's size has not been computed
     *                               yet
     */
    public int height() {
        if (this.height < 0)
            throw new IllegalStateException("The size hasn't been computed yet");
        else
            return height;
    }

    /**
     * Gets the matrix's width
     * 
     * @return The matrix's width
     * @throws IllegalStateException When the matrix's size has not been computed
     *                               yet
     */
    public int width() {
        if (this.width < 0)
            throw new IllegalStateException("The size hasn't been computed yet");
        else
            return width;
    }

    /**
     * Returns an specific pixel on the matrix
     * @param x The x position of the pixel
     * @param y The y position of the pixel
     * @return The pixel of the piece's matrix at the specified position
     * @throws ArrayIndexOutOfBoundsException If the point specified is out of the bounds of the matrix
     */
    public T getPixel(int x, int y) throws ArrayIndexOutOfBoundsException {
        return matrix[y][x];
    }
}