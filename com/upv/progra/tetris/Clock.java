package com.upv.progra.tetris;

import java.util.Timer;
import java.util.TimerTask;

public class Clock extends TimerTask {
    private Runnable callback;
    private Timer timer;

    private int fps = 0;
    private int ticks = 0;

    private boolean isStarted = false;

    public void run() {
        ticks++;
        int toTicks = 1000 / fps;
        if (ticks >= toTicks) {
            callback.run();
            ticks = 0;
        }
    }

    public Clock(int fps, Runnable callback) {
        this.callback = callback;
        this.fps = fps;
    }

    private void schedule() {
        timer = new Timer();
        timer.schedule(this, 0, 1);
    }

    public void start() throws IllegalStateException {
        if (!isStarted)
            schedule();
        else
            throw new IllegalStateException("The timer has already been started");
    }

    public boolean isStarted() {
        return isStarted;
    }

    public void setFps(int fps) {
        this.fps = fps;
    }

    public static Clock start(int fps, Runnable callback) {
        Clock task = new Clock(fps, callback);
        task.start();
        return task;
    }
}