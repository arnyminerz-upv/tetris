package com.upv.progra.tetris;

import java.awt.*;

/**
 * The class model for all the pieces, some variables and functions must be
 * overriden.
 * 
 * @author Arnau Mora
 * @version 1
 */
public abstract class Piece extends Matrix<Boolean> {
    /**
     * The color of the piece
     */
    public Color color;

    /**
     * The piece's position
     */
    public Point position;

    @Override
    public void log() {
        Debugger.println(Debugger.DEBUG_INFO, "Color: (%d, %d, %d)", color.getRed(), color.getGreen(), color.getBlue());
        super.log();
    }

    private Piece(Piece piece) {
        super(piece);
        this.color = piece.color;
        this.position = new Point(0, 0);
    }

    protected Piece(Boolean pattern[][], Color color) {
        super(Boolean.class, pattern, false);
        this.color = color;
        this.position = new Point(0, 0);
    }

    /**
     * Draws the piece in the graphics
     * 
     * @param g The graphics to draw on
     */
    public void draw(Graphics g) {
        Debugger.println(Debugger.DEBUG_VERBOSE, "Drawing %s...", getClass().getSimpleName());
        g.setColor(color);
        for (int y = 0; y < length(); y++) {
            Boolean[] row = get(y);
            for (int x = 0; x < row.length; x++) {
                if (row[x])
                    g.fillRect(position.getX(Tetris.BLOCK_SIZE) + x * Tetris.BLOCK_SIZE,
                            position.getY(Tetris.BLOCK_SIZE) + y * Tetris.BLOCK_SIZE, Tetris.BLOCK_SIZE,
                            Tetris.BLOCK_SIZE);
            }
        }
    }

    /**
     * Moves the piece one position down
     */
    public void moveDown() {
        move(0, 1);
    }

    /**
     * Moves the piece one position left
     */
    public void moveLeft() {
        move(-1, 0);
    }

    /**
     * Moves the piece one position right
     */
    public void moveRight() {
        move(1, 0);
    }

    /**
     * Moves the piece to the relative position set
     * 
     * @param x The relative x position
     * @param y The relative y position
     */
    public void move(int x, int y) {
        position.addX(x);
        position.addY(y);
        position.constrainX(0, Tetris.GAME_WIDTH - width());
        position.constrainY(0, Tetris.GAME_HEIGHT - height());
    }

    /**
     * Creates a copy of the class
     * 
     * @return A clone of this class
     */
    public abstract Piece create();
}
