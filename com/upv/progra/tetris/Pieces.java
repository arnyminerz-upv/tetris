package com.upv.progra.tetris;

import com.upv.progra.tetris.pieces.*;

public class Pieces {
    /**
     * All the available pieces
     */
    public static final Piece PIECES[] = { new IPiece(), new JPiece(), new LPiece(), new OPiece(), new SPiece(),
            new TPiece(), new ZPiece() };

    /**
     * Generates a random piece
     * 
     * @return A random piece
     */
    public static Piece random() {
        int index = Utils.getRandomNumber(0, PIECES.length - 1);
        Piece piece = PIECES[index];
        return piece.create();
    }

    /**
     * Computes the sum of the widths of all the pieces
     * 
     * @param pieces The pieces
     * @return The sum of the widths of all the pieces
     */
    public static int width(Piece... pieces) {
        int width = 0;
        for (Piece piece : pieces)
            width += piece.position.getX() + piece.width();
        return width;
    }

    /**
     * Computes the sum of the heights of all the pieces
     * 
     * @param pieces The pieces
     * @return The sum of the heights of all the pieces
     */
    public static int height(Piece... pieces) {
        int height = 0;
        for (Piece piece : pieces)
            height += piece.position.getY() + piece.height();
        return height;
    }

    /**
     * Checks if two pieces collide
     * 
     * @return If the two pieces collide
     */
    public static boolean collide(Piece... pieces) {
        // If there's only one piece, no collision can happen
        if (pieces.length <= 1)
            return false;

        // First, reconstruct the current situation
        int maxWidth = width(pieces);
        int maxHeight = height(pieces);
        // Instantiate the construction matrix with max height and width to avoid
        // ArrayIndexOutOfBoundsException
        boolean newMatrix[][] = new boolean[maxHeight][maxWidth];

        for (Piece piece : pieces) {
            // Do the calcs with height and width, to optimize
            int height = piece.height(), width = piece.width();
            // Get the pieces position and sums
            int startY = piece.position.getY();
            int startX = piece.position.getX();
            int endY = startY + height;
            int endX = startX + width;
            // Copy the pixels to the new matrix
            for (int y = startY; y < endY; y++) {
                for (int x = startX; x < endX; x++) {
                    int compX = x - startX;
                    int compY = y - startY;
                    Debugger.print(Debugger.DEBUG_VERBOSE, newMatrix[y][x] ? "#" : " ");
                    // If there's already a pixel, the matrixes collide
                    if (newMatrix[y][x] == true)
                        // Return, no more search needs to be done
                        return true;
                    else
                        // If not, store the current pixel into the matrix
                        newMatrix[y][x] = piece.getPixel(compX, compY);
                }
                Debugger.ln(Debugger.DEBUG_VERBOSE);
            }
            Debugger.ln(Debugger.DEBUG_VERBOSE);
        }

        return false;
    }
}
