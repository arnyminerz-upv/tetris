package com.upv.progra.tetris.exception;

public class PiecesCollideException extends Exception {
    private static final long serialVersionUID = 5343334968930273595L;

    public PiecesCollideException() {
        super("The two pieces collide.");
    }
}
