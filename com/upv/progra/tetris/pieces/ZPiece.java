package com.upv.progra.tetris.pieces;

import com.upv.progra.tetris.Colors;
import com.upv.progra.tetris.Piece;

/**
 * A piece that defines the Z piece.
 * 
 * @see com.upv.progra.tetris.Piece
 * @author Arnau Mora
 * @version 1
 */
public class ZPiece extends Piece {
    /**
     * The class constructor.
     * Doesn't have the pattern parameter since it is
     *   implicit in the ZPiece class name.
     */
    public ZPiece() {
        super(new Boolean[][] {
            { true, true, false }, // 1 1 0
            { false, true, true }  // 0 1 1
        }, Colors.RED);
    }

    @Override
    public Piece create() {
        return new ZPiece();
    }
}
