package com.upv.progra.tetris.pieces;

import com.upv.progra.tetris.Colors;
import com.upv.progra.tetris.Piece;

/**
 * A piece that defines the T piece.
 * 
 * @see com.upv.progra.tetris.Piece
 * @author Arnau Mora
 * @version 1
 */
public class TPiece extends Piece {
    /**
     * The class constructor.
     * Doesn't have the pattern parameter since it is
     *   implicit in the TPiece class name.
     */
    public TPiece() {
        super(new Boolean[][] {
            { false, true, false }, // 0 1 0
            { true, true, true }    // 1 1 1
        }, Colors.PURPLE);
    }

    @Override
    public Piece create() {
        return new TPiece();
    }
}
