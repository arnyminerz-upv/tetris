package com.upv.progra.tetris.pieces;

import com.upv.progra.tetris.Colors;
import com.upv.progra.tetris.Piece;

/**
 * A piece that defines the S piece.
 * 
 * @see com.upv.progra.tetris.Piece
 * @author Arnau Mora
 * @version 1
 */
public class SPiece extends Piece {
    /**
     * The class constructor.
     * Doesn't have the pattern parameter since it is
     *   implicit in the SPiece class name.
     */
    public SPiece() {
        super(new Boolean[][] {
            { false, true, true }, // 0 1 1
            { true, true, false }  // 1 1 0
        }, Colors.GREEN);
    }

    @Override
    public Piece create() {
        return new SPiece();
    }
}
