package com.upv.progra.tetris.pieces;

import com.upv.progra.tetris.Colors;
import com.upv.progra.tetris.Piece;

/**
 * A piece that defines the O piece.
 * 
 * @see com.upv.progra.tetris.Piece
 * @author Arnau Mora
 * @version 1
 */
public class OPiece extends Piece {
    /**
     * The class constructor.
     * Doesn't have the pattern parameter since it is
     *   implicit in the OPiece class name.
     */
    public OPiece() {
        super(new Boolean[][] {
            { true, true }, // 1 1
            { true, true }  // 1 1
        }, Colors.YELLOW);
    }

    @Override
    public Piece create() {
        return new OPiece();
    }
}
