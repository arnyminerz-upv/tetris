package com.upv.progra.tetris.pieces;

import com.upv.progra.tetris.Colors;
import com.upv.progra.tetris.Piece;

/**
 * A piece that defines the L piece.
 * 
 * @see com.upv.progra.tetris.Piece
 * @author Arnau Mora
 * @version 1
 */
public class LPiece extends Piece {
    /**
     * The class constructor.
     * Doesn't have the pattern parameter since it is
     *   implicit in the LPiece class name.
     */
    public LPiece() {
        super(new Boolean[][] {
            { true, false }, // 1 0
            { true, false }, // 1 0
            { true, true } // 1 1
        }, Colors.ORANGE);
    }

    @Override
    public Piece create() {
        return new LPiece();
    }
}
