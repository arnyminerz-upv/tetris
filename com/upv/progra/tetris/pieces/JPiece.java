package com.upv.progra.tetris.pieces;

import com.upv.progra.tetris.Colors;
import com.upv.progra.tetris.Piece;

/**
 * A piece that defines the J piece.
 * 
 * @see com.upv.progra.tetris.Piece
 * @author Arnau Mora
 * @version 1
 */
public class JPiece extends Piece {
    /**
     * The class constructor.
     * Doesn't have the pattern parameter since it is
     *   implicit in the JPiece class name.
     */
    public JPiece() {
        super(new Boolean[][] {
            { false, true }, // 0 1
            { false, true }, // 0 1
            { true, true }   // 1 1
        }, Colors.BLUE);
    }

    @Override
    public Piece create() {
        return new JPiece();
    }
}
