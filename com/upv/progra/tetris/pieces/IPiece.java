package com.upv.progra.tetris.pieces;

import com.upv.progra.tetris.Colors;
import com.upv.progra.tetris.Piece;

/**
 * A piece that defines the I piece.
 * 
 * @see com.upv.progra.tetris.Piece
 * @author Arnau Mora
 * @version 1
 */
public class IPiece extends Piece {
    /**
     * The class constructor.
     * Doesn't have the pattern parameter since it is
     *   implicit in the IPiece class name.
     */
    public IPiece() {
        super(new Boolean[][] {
            { true }, // 1
            { true }, // 1
            { true }, // 1
            { true }  // 1
        }, Colors.LIGHT_BLUE);
    }

    @Override
    public Piece create() {
        return new IPiece();
    }
}
