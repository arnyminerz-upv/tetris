package com.upv.progra.tetris;

import java.awt.*;

public class Colors {
    public static final Color LIGHT_BLUE = new Color(0x10D1EF);
    public static final Color BLUE = new Color(0x0131AE);
    public static final Color GREEN = new Color(0x72CB3B);
    public static final Color YELLOW = new Color(0xFFD500);
    public static final Color ORANGE = new Color(0xFF971C);
    public static final Color RED = new Color(0xFF3213);
    public static final Color PURPLE = new Color(0xB710EF);
}
