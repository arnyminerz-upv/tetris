package com.upv.progra.tetris;

public class Debugger {
    public static final int DEBUG_VERBOSE = 0;
    public static final int DEBUG_INFO = 1;
    public static final int DEBUG_WARNING = 2;
    public static final int DEBUG_ERROR = 3;

    private static int DEBUG_LEVEL = DEBUG_VERBOSE;

    /**
     * Sets a new debug level
     * @param newLevel The new level of debug
     */
    public static void setDebugLevel(int newLevel) {
        DEBUG_LEVEL = newLevel;
    }

    /**
     * Gets the current debug level
     * @return The current debug level
     */
    public static int getDebugLevel(){
        return DEBUG_LEVEL;
    }

    /**
     * Prints through the terminal a message
     * 
     * @param message The message to print
     */
    public static void print(String message) {
        System.out.print(message);
    }

    /**
     * Prints through the terminal a message, and replaces the args placeholders.
     * 
     * @param message The message to print
     * @param args    The arguments to replace in the message
     */
    public static void print(String message, Object... args) {
        System.out.printf(message, args);
    }

    /**
     * Prints through the terminal a message only if the current debug level is less
     * or equal than level
     * 
     * @param message The message to print
     * @param level   The level of debug
     */
    public static void print(int level, String message) {
        if (level >= DEBUG_LEVEL)
            System.out.print(message);
    }

    /**
     * Prints through the terminal a message only if the current debug level is less
     * or equal than level, and replaces the args placeholders.
     * 
     * @param message The message to print
     * @param args    The arguments to replace in the message
     * @param level   The level of debug
     */
    public static void print(int level, String message, Object... args) {
        if (level >= DEBUG_LEVEL)
            System.out.printf(message, args);
    }

    /**
     * Prints through the terminal a message
     * 
     * @param message The message to print
     */
    public static void println(String message) {
        System.out.println(message);
    }

    /**
     * Prints through the terminal a message, and replaces the args placeholders.
     * 
     * @param message The message to print
     * @param args    The arguments to replace in the message
     */
    public static void println(String message, Object... args) {
        System.out.printf(message + '\n', args);
    }

    /**
     * Prints through the terminal a message only if the current debug level is less
     * or equal than level
     * 
     * @param message The message to print
     * @param level   The level of debug
     */
    public static void println(int level, String message) {
        if (level >= DEBUG_LEVEL)
            System.out.println(message);
    }

    /**
     * Prints through the terminal a message only if the current debug level is less
     * or equal than level, and replaces the args placeholders.
     * 
     * @param message The message to print
     * @param args    The arguments to replace in the message
     * @param level   The level of debug
     */
    public static void println(int level, String message, Object... args) {
        if (level >= DEBUG_LEVEL)
            System.out.printf(message + '\n', args);
    }

    /**
     * Prints a line jump
     */
    public static void println() {
        System.out.println();
    }

    /**
     * An alias for println
     */
    public static void ln() {
        println();
    }

    /**
     * Prints a line jump.
     * 
     * @param level The debug level
     */
    public static void println(int level) {
        if (level >= DEBUG_LEVEL)
            println();
    }

    /**
     * An alias for println
     * 
     * @param level The debug level
     */
    public static void ln(int level) {
        println(level);
    }
}
