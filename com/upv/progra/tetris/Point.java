package com.upv.progra.tetris;

/**
 * A class for a bidimensional array
 * 
 * @author Arnau Mora
 * @version 1
 */
public class Point {
    private int x;
    private int y;

    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    // Declare getters and setters

    /**
     * Sets the x value of the point
     * 
     * @param x The x value
     * @since 1
     */
    public void setX(int x) {
        this.x = x;
    }

    /**
     * Gets the x value of the point
     * 
     * @return The x value of the point
     * @since 1
     */
    public int getX() {
        return this.x;
    }

    /**
     * Gets the x value of the point
     * 
     * @param blockSize The point size
     * @return The x value of the point
     * @since 1
     */
    public int getX(int blockSize) {
        return this.x * blockSize;
    }

    /**
     * Adds a value to the x position
     * 
     * @param add The amount to add
     */
    public void addX(int add) {
        this.x += add;
    }

    /**
     * Forces the X value to be between two others
     */
    public void constrainX(int min, int max) {
        if (x < min)
            x = min;
        if (x > max)
            x = max;
    }


    /**
     * Sets the y value of the point
     * 
     * @param y The y value
     * @since 1
     */
    public void setY(int y) {
        this.y = y;
    }

    /**
     * Gets the y value of the point
     * 
     * @return The y value of the point
     * @since 1
     */
    public int getY() {
        return this.y;
    }

    /**
     * Gets the y value of the point
     * 
     * @param blockSize The point size
     * @return The y value of the point
     * @since 1
     */
    public int getY(int blockSize) {
        return this.y * blockSize;
    }

    /**
     * Adds a value to the y position
     * 
     * @param add The amount to add
     */
    public void addY(int add) {
        this.y += add;
    }
    
    /**
     * Forces the X value to be between two others
     */
    public void constrainY(int min, int max) {
        if (y < min)
            y = min;
        if (y > max)
            y = max;
    }
}
