package com.upv.progra.tetris;

import java.util.ArrayList;

import javax.swing.JPanel;

public class Board {
    private JPanel panel;

    int currentPiece = -1;
    private ArrayList<Piece> pieces;

    public Board(JPanel panel) {
        this.panel = panel;
        pieces = new ArrayList<Piece>();
    }

    /**
     * Creates a new piece
     */
    public void createNewPiece() {
        currentPiece = pieces.size();
        pieces.add(Pieces.random());
    }

    /**
     * Moves the current piece one position to the right
     */
    public void moveRight() {
        Piece currentPiece = pieces.get(this.currentPiece);
        if (emulateMovement(currentPiece, 1, 0) == MOVEMENT_OK) {
            currentPiece.moveRight();
            draw();
        }
    }

    /**
     * Moves the current piece one position to the left
     */
    public void moveLeft() {
        Piece currentPiece = pieces.get(this.currentPiece);
        if (emulateMovement(currentPiece, -1, 0) == MOVEMENT_OK) {
            currentPiece.moveLeft();
            draw();
        }
    }

    /**
     * Rotates the current piece
     */
    public void rotate() {
        Piece currentPiece = pieces.get(this.currentPiece);
        currentPiece.rotate();
        draw();
    }

    /**
     * Clears the board
     */
    public void clear() {
        panel.getGraphics().clearRect(0, 0, Tetris.GAME_WIDTH * Tetris.BLOCK_SIZE,
                Tetris.GAME_HEIGHT * Tetris.BLOCK_SIZE);
    }

    /**
     * Redraws all the pieces
     */
    public void draw() {
        clear();
        // Draw all pieces
        for (Piece piece : pieces)
            piece.draw(panel.getGraphics());
    }

    /**
     * Runs a tick of the board
     */
    public void tick() {
        Piece currentPiece = pieces.get(this.currentPiece);

        currentPiece.log();
        if (currentPiece.position.getY() + 1 + currentPiece.height() >= Tetris.GAME_HEIGHT)
            createNewPiece();
        else if (emulateMovement(currentPiece, 0, 1) == MOVEMENT_OK)
            currentPiece.moveDown();
        else
            createNewPiece();

        draw();
    }

    public static int MOVEMENT_OK = 0;
    public static int MOVEMENT_COLLIDED = 1;
    public static int MOVEMENT_CHECK_FAILED = -1;

    private int emulateMovement(Piece piece, int x, int y) {
        Debugger.println(Debugger.DEBUG_INFO, "Emulating movement for piece %s...", piece.getClass().getSimpleName());

        int piecesCount = pieces.size();
        // Emulate the movement
        piece.move(x, y);
        // Get a list of all the pieces, changing the current one by the virtual
        ArrayList<Piece> pieces = new ArrayList<Piece>();
        if (piecesCount > 1)
            pieces.addAll(this.pieces.subList(0, piecesCount - 2));
        pieces.add(piece);
        // Check if there's any collision, we assume that all the pieces were fine
        // before running the collide.
        boolean collide = Pieces.collide(pieces.toArray(new Piece[pieces.size()]));
        // Return the piece to its original location
        piece.move(-x, -y);

        if (collide){
            Debugger.println(Debugger.DEBUG_INFO, "  The piece collided!");
            return MOVEMENT_COLLIDED;
        }
        Debugger.println(Debugger.DEBUG_INFO, "  The piece can be moved :)");
        return MOVEMENT_OK;
    }
}
